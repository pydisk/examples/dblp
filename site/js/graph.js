default_graph = {
		"radius":10,
		"charge_strength": -250,
		"link_power": 2.5,
		"link_lin" : 0.15,
		"link_strength":(e, p)=>Math.pow(e, p),
		"opacity_corr":(e)=>1.1*Math.pow(e,2.5),
		"height": window.innerHeight*0.8,
		"width": window.innerWidth*0.8,
		"node_map":new Map(),
    "link_stroke": (d)=> d.weight==1? (d.type == "primary"? "#007bff":"#17a2b8"):"#555",
		"link_list": []
	};

function zoom_actions(){
            g.attr("transform", d3.event.transform)
}

function drag(simulation){
    function dragstarted(event) {
		if (!event.active) simulation.alphaTarget(0.3).restart();
		event.subject.fx = event.subject.x;
		event.subject.fy = event.subject.y;
    }   
    function dragged(event) {
		event.subject.fx = event.x;
		event.subject.fy = event.y;
    }   
    function dragended(event) {
		if (!event.active) simulation.alphaTarget(0);
		event.subject.fx = null;
		event.subject.fy = null;
    }   
    return d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended);
}

function link_sel(event){
	event.target.style.stroke = "red";
	event.target.style.strokeOpacity = 1;
}
function link_unsel(graph, event){
    event.target.style.stroke = graph.link_stroke(event.target.__data__);	
    event.target.style.strokeOpacity = 1/graph.opacity_corr(event.target.__data__.weight);
}
function link_click(graph, event){
    var link = event.target.__data__;
    graph.on_link_click(link);
}

function create_graph(element, graph_property){
	var graph = default_graph;
	graph.element_radius = function(d){
		if (d == null || d.radius == null)
			return this.radius;
		else
			return d.radius
	}
	if (graph_property != null) Object.assign(graph, graph_property);

	var svg = d3.select(element).append("svg")
			.attr("width", graph.width)
			.attr("height", graph.height)
			.attr("class", "draw_graph");
	graph.svg = svg;
    var g = graph.g = graph.svg.append("g");

    graph.on_link_click = function(link){};
    graph.on_node_click = function(node){};

	graph.update_canvas_size = function(){
		this.height = window.innerHeight*0.8,
		this.width  = window.innerWidth*0.8,
		this.svg.attr("width", this.width)
			.attr("height", this.height);
		this.restart();
	}
	graph.refresh_node = function(){

		var node = this.g.selectAll(".nodeg").data([]);
		node.exit().remove();

		var node = this.g.selectAll(".nodeg").data(Array.from(this.node_map.values()));

		var node_g = node.enter()	
				.append("g")
				.attr("class", "nodeg")
                .on("dblclick", (event) =>{
                  this.on_node_click(event.target.__data__)
                  event.stopPropagation();
                  }
                );
		node_g.append("circle")
   				.attr("class", (d) => d.type)
				.attr("r", (d) => this.element_radius(d));
		node_g.filter((d) => d.name != null).append("text")
				.text((d)=>d.name)
				.attr("x", 7)
				.attr("y", -7)
				.attr('class', 'node_label')
		node_g.call(drag(simulation));
		this.node = node_g;
		return node_g;
	}
	graph.filter_link = (link) => true;

	graph.refresh_link = function(){
        var that = this;
		var link = this.g.selectAll(".link").data([]);
		link.exit().remove();
		this.filtered_links = this.link_list.filter(this.filter_link);
		link = this.g.selectAll(".link").data(this.filtered_links);
		link = link.enter().append("line")
			.attr("class", "link")
			.attr("stroke-opacity", function(d){
				return 1/graph.opacity_corr(d.weight);
			})
			.attr("stroke", this.link_stroke)
			.attr("stroke-width", (d)=> d.weight==1?"3px":"3px")
			.on("mouseover", link_sel)
			.on("mouseout", (event) => link_unsel(that, event))
			.on("click", (event) => link_click(that, event))
			.merge(link);
		return link;
	}
	var node = graph.refresh_node();
	var link = graph.refresh_link();

	var simulation = d3.forceSimulation(Array.from(graph.node_map.values()))
		.force('link', d3.forceLink().links(link));

	graph.link_strength_comp = (d) =>  graph.link_lin * graph.link_strength(1/d.weight, graph.link_power);
	graph.simulation = simulation;
	graph.restart = function(){
		var that = this;
		var link = this.refresh_link();
		var node = this.refresh_node();
		this.simulation.nodes(Array.from(this.node_map.values()))
			.force('link').links(this.filtered_links).strength(that.link_strength_comp);
		this.simulation
	  		.force('charge', d3.forceManyBody().strength(()=>that.charge_strength))
			.force('center', d3.forceCenter(this.width / 2, this.height / 2))
			.on('tick', function(){
		    link.attr("x1", function(d) { return d.source.x; })
	    	    .attr("y1", function(d) { return d.source.y; })
	        	.attr("x2", function(d) { return d.target.x; })
		        .attr("y2", function(d) { return d.target.y; });
			node.attr("transform", function(d) { 
					return "translate(" + d.x + "," + d.y + ")"; 
			});
	  });
		try{
			this.simulation.alpha(1).restart();
		} catch(error){
			console.log("When restarting simulation", error);
			this.simulation.stop();
		}

	}
	graph.add_node = function (node_data){
		console.log("Add node", node_data);
		node_data.x = Math.random() * this.width;
		node_data.y = Math.random() * this.height;
		if(!this.node_map.has(parseInt(node_data["id"]))){
			this.node_map.set(parseInt(node_data["id"]), node_data);
			this.restart()
		}
	}
	graph.add_link = function(n1, n2, weight, type){
		/*
		nn1 = Math.min(n1, n2);
		n2 = Math.max(n1, n2);
		nn1 = n1;*/
		console.log("Add link", n1, n2, weight);
		if (!(this.node_map.has(n1) && this.node_map.has(n2))){
			return;
		}
		nn1 = Math.min(n1, n2);
		n2 = Math.max(n1, n2);
		n1 = nn1;
		if (this.node_map.has(n1)&& this.node_map.has(n2)){
			this.link_list.push({"source":this.node_map.get(n1), "target":this.node_map.get(n2), "weight":weight, "type":type})
		}
		this.restart();
	}
    graph.remove_link = function(link){
        this.link_list.splice(this.link_list.indexOf(link), 1);
        this.restart();
    }
	graph.restart();
    const zoom = d3.zoom().scaleExtent([0.1, 4])
        .on("zoom", (event) => { g.attr("transform", event.transform) });

    svg.call(zoom);
	return graph;
}
