function TrackedArray(){
	Arr = [];
	Arr.onpush = function(){};
	Arr.onclear = function(){};
	Arr.onpop = function(){};
	Arr.onremove = function(){};
	Arr.push = function(e){
		Array.prototype.push.call(this, e);
		this.onpush(this.length, e);
	}
	Arr.clear = function(){
		this.length = 0;
		this.onclear();
	}
	Arr.pop = function (){
		Array.prototype.push.call(this, e);
		this.onpop(this.length);
	}
	Arr.remove = function(element){
		index = this.indexOf(element);
		while (index > -1){
			this.splice(index, 1);
			index = this.indexOf(element);
		}
		this.onremove();
	}
	return Arr;
}
function temp_alert(container, text, callback){
	var span = document.createElement("div");
	span.innerHTML = text;
	span.setAttribute("class", "alert-span");
	container.appendChild(span);
	span.onclick = function(){ this.remove()};
}
  
function utf8_to_b64( str ) {
	return window.btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8( str ) {
	return decodeURIComponent(escape(window.atob( str )));
}

function cust_alert(msg){
	var alert_cont = `
<div class="alert alert-danger d-flex align-items-center" role="alert" id="alert">
	 <svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#exclamation-triangle-fill"/></svg>
	 <div style="margin-left:2em">${msg}</div>
</div>`;
	var alertEl = document.createElement("div");
	alertEl.innerHTML = alert_cont;
	alertEl.style.maxWidth = "50%";
	alertEl.style.margin = "auto";
	var main = document.getElementById("main");
	document.body.insertBefore(alertEl, main);
}
