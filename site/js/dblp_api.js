function prototype_api_access(fname, args){
	return new Promise(function(Resolve, Reject){
		var request = new XMLHttpRequest();
		if (args != null)
			var method = "POST";
		else{
			var method = "GET";
			args = Object();
		}
		request.open(method, "/dblp/api/"+fname);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.onload = function() {
			switch (request.status){
				case 200: Resolve(JSON.parse(request.response));
				break;
				case 202:{
					obj = JSON.parse(request.response);
					Reject(obj);
				}
				break;
				default: Reject(new Error("Invalid status code", request.status))
			}
		}
		request.send(Object.entries(args).map(e=>e.join("=")).join("&"));
	});
}
search_author = (name) => prototype_api_access("find_author", {"name":name});
search_authors = (array_name) => prototype_api_access("find_authors", {"names":array_name.join(",")});
get_nodes_data = (array_id) => prototype_api_access("get_nodes_data", {"ids":array_id.join(",")});
get_coauthors = (id) => prototype_api_access("get_coauthors", {"id":id});
get_node_data = (id) => prototype_api_access("get_node_data", {"id":id});
get_neighbors = (id) => prototype_api_access("get_neighbors", {"id":id});
shortest_path = (source, target) => prototype_api_access("shortest_path", {"source":source, "target":target});
is_alive = () => prototype_api_access("is_alive");
kv_get = (key) => prototype_api_access("kv_store",{"key":key});
kv_set = (key, value) => prototype_api_access("kv_store", {"key": key, "value": value});
kv_save = (value) => prototype_api_access("kv_store", {"value":value});

