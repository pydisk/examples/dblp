data = {}; // data contains id: node_data information
named_key = {}; // mapp from name authors to id
selected_authors = TrackedArray();
relations = {};
window.onresize = function(){
	graph.update_canvas_size();
}
window.onload = function(){
	var param = window.location.search.split("?")[1];
	if (param != null && param.length>0){
		kv_get(param).then(function(data){
			var names = JSON.parse(data["value"]);
			names.forEach(add_author);
		});
	}
	is_alive().catch(()=> cust_alert("The server seems to not be available at the moment. Contact the administrator (charles.paperman@univ-lille.fr)"))
	container = document.getElementById("svg_container");
	graph = generate_d3_graph();
	save_butt.onclick = () => save_current_graph();
	add_author_input.addEventListener("keyup", function(){
		if (event.keyCode === 13) {
			event.preventDefault();
			add_author_butt.click();
		}
	});
	add_author_butt.onclick = function(){
		var values = add_author_input.value.split(",");
		values = values.map((e)=>e.trim());
		add_author_input.value = "";
		values.forEach( (value) => 
			add_author(value).catch(function(err){
		                console.log(err);
				parentE = add_author_feedback;
				add_author_input.setAttribute("class", "is-invalid")
				temp_alert(parentE, `"${value}" is not in the Database.`, ()=> add_author_input.classList.remove("is-invalide"));
		}));
	};
}

function NodesNotFound(name, err){
	console.log(name, "not found in the database", err);
}

function IdDataNotFound(id, err){
	console.log("No data found for", id, err);
}

function ShortestPathError(id1,id2, err){
	console.log("Error during shortest path computation:", id1, id2, err);
}

function add_author(name){
	return new Promise(function (resolve, reject){
        d = named_key[name];
		if (d != null) {
            d.radius = 10;
            d.type = "primary";
			resolve(d);
            if (selected_authors.indexOf(d.id) == -1)
	    		selected_authors.push(d.id);
		}
		else search_author(name)
			.then(function(e){
				console.log("Author find", name, e);
				get_node_data([e.node_id])
					.then(function(f){
						data[e.node_id] = f.data;
						data[e.node_id].id = e.node_id;
            data.radius = 10;
            data.type = "primary";
   					named_key[name] = e.node_id;	
						relations[e.node_id] = {};
						selected_authors.push(e.node_id);
						resolve(e.node_id);
					}).catch(reject);
			}).catch(reject);
	});
}

function add_bunch_nodes(array_id, callback){
	if (callback == null) callback = function(){};
	get_nodes_data(array_id).then(function(new_data){
			Object.assign(data, new_data.nodes_data)
			Object.keys(data).forEach( e=> { 
                    var id = parseInt(e);
                    Object.assign(data[e], {"id":id});
                    named_key[data[e]["name"]] = data[e];
                    if (relations[id] == null)
                        relations[id] = {};
             });
			callback(array_id.map((id)=>data[id]))
	});
}

function connect_id(id1, id2, callback){
		if (callback == null) callback=()=>{};
		if (relations[id1][id2] == null && id1!=id2){
			relations[id1][id2] = false;
			relations[id2][id1] = false;

			console.log("Computing shortest path between", id1, id2);
			shortest_path(id1, id2).then(
				data=> {
					console.log("Shortest Path computed between", id1, id2);
					add_bunch_nodes(data.path);
					relations[id1][id2] = data.path;
					relations[id2][id1] = data.path;
					callback(data);
				}
			).catch((err)=> ShortestPathError(id1, id2, err));
		}
}
/*

We compute relation each time we add a node to selected author.
 Relation are cached, so it will not generate extra queries to call
 it multiple time.

 */
function add_url(value){
	var a = document.createElement("a");
	a.href = `index.html?${value}`;
	a.setAttribute("class", "graph_url");
	a.innerHTML = value;
	a.setAttribute("class", "list-group-item");
	urlcontainer.appendChild(a);
}

function save_current_graph(name){
	var name_list = selected_authors.map((e)=>data[e].name);
	
	var content = JSON.stringify(name_list);
 	if (name == null || name.length == 0)
		kv_save(content).then((d)=> add_url(d["key"]));
	
	else
		kv_set(name, content).then(()=> add_url(name));
	
}
selected_authors.onpush = function(length, target) { 
	graph.add_node(data[target]);
	this.slice(0, length).forEach(
			(source)=> connect_id(
					source, 
					target,
					(res) => graph.add_link(source, target, (res.path.length-1)/2, "primary"))
	);
    
};

function get_graph_data(array_names){
		container.innerHTML = "";
		selected_authors.clear();
		array_names.forEach((e) =>  add_author(e));
}

function generate_d3_graph(graph_prop){
	container.innerHTML = "";
	graph = {};
	Object.assign(graph, graph_prop);
	graph = create_graph(container, graph);
  graph.filter_link = function(link){
        if (link.type == "primary"){
            var path = relations[link.source.id][link.target.id];
            if (typeof path == "boolean") return true;
            if (path.length == 3) return true;
            var fpath = path.filter(function(index){
                if (selected_authors.indexOf(index) != -1 && data[index] != null)
                    if (data[index].name != null)
                        return true;
                return false;
             });
            if (fpath.length == 0) 
                return false;
        }
        return true;
    }
  graph.on_node_click = (d) => add_author(d.name);
  graph.on_link_click = function(link){
  	if (link["type"] == "primary"){
  	   this.remove_link(link);
  	   var path = relations[link.source.id][link.target.id];
		 	 add_bunch_nodes(path, function(array_id){
		 			 array_id.forEach(function(d, index){
  	       	 if (d.name != null){
  	       	     if (selected_authors.indexOf(d.id) == -1){
  	       	    	 d["type"] = "secondary_author";
  	       	    	 d["path"] = path;
  	       	    	 d["radius"] = 5;
  	       	    	 graph.add_node(d);
  	       	     }
  	       	     if (index > 0){
  	       	         var n1 = path[index-2];
  	       	         var n2 = path[index];
  	       	         var type = "secondary";
  	       	         if (selected_authors.indexOf(n1) != -1 && selected_authors.indexOf(n2) != -1)
  	       	             type = "primary";
  	       	         graph.add_link(path[index-2], path[index], 1, type);
  	       	    }
  	        	}
		 			});
		 	});
		}
	}
  return graph
}


