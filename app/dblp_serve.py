from flask import Flask, request, g
from functools import wraps
import re, json, time, sqlite3, os, copy, random, uuid, getpass
import networkdisk as nd
import networkx as nx

path = __file__.rsplit("/", maxsplit=1)[0]
app = Flask(__name__)
db = f"{path}/../data/dblp.db"
#db = "/media/datas/home-data/networkdisk/examples/dblp/last/dblp.fts.db"
schema = nd.sqlite.Graph(db=db).schema
kvpath = f"{path}/kv.db"
dblp = nd.sqlite.Graph(db=db, static=True, edge_cache_level=2)

def get_dblp():
    return dblp

wildcard = re.compile(r".*[^\\]?(%|_).*")
def wildcard_sub(value):
    if wildcard.match(value):
        return lambda c:c.like(value)
    return value

def unify(fct):
    @wraps(fct)
    def dec_fct():
        start = time.time()
        dblp = get_dblp()
        end_dblp_load = time.time() - start
        if request.method == "POST":
            data = request.form
        else:
           data = request.args

        start = time.time()
        qc = dblp.helper.sql_logger.querycount
        print("Executing fct:", fct.__name__)
        try:
            d = fct(dblp, data)
            status = 200
        except Exception as E:
            d = { "Exception": str(E) } 
            status = 202 
        qc = dblp.helper.sql_logger.querycount - qc
        d.update(meta=dict( 
            input=dict(data),
            query=[dict(query_str=e.qformat(), query_arg=list(map(repr, e.get_subargs()))) for e in list(dblp.helper.sql_logger.buffer)[-qc:]],
            time=time.time() - start,
            query_count=qc,
            dblp_load_time = end_dblp_load
            ))
        return d, status
    return dec_fct

@app.route('/is_alive')
def is_alive():
    return dict(msg="I am Alive!")

@app.route("/info")
@unify
def infos(dblp, data):
    return dict(
            path=__file__,
            nd_version=nd.__version__,
            nx_version=nx.__version__,
            key_value_data_path=kvpath,
            graph_loaded=dblp.helper.dbpath, 
            homedir=os.path.expanduser("~"), 
            user=getpass.getuser()
            )

@app.route("/find_authors", methods=["POST", "GET"])
@unify
def find_authors(dblp, data):
    names = data.get("names", None)
    if not names:
        raise ValueError("Must provid as leat a name. Shape of the query names=name1,name2,...,namek")
    return dict(nodes_id=list(dblp.find_all_nodes("name", name=lambda e:e.inset(*map(str.strip, names.split(","))))))

@app.route("/find_author", methods=["POST", "GET"])
@unify
def find_author(dblp, data):
    name = data.get("name", None)
    if not name:
        raise ValueError("Must provid as leat a name. Shape of the query name=some_name")
    return dict(node_id=dblp.find_one_node("name", name=wildcard_sub(name)))
  

@app.route("/find_all", methods=["POST", "GET"])
@unify
def find_all(dblp, data):
    return dict(ids=list(dblp.find_all_nodes(
                    *tuple(data),
                    **{k:wildcard_sub(v) for k,v in data.items()})))
@app.route("/get_nodes_data", methods=["POST", "GET"])
@unify
def get_data(dblp, data):
    ids = list(map(int, data["ids"].split(",")))
    d = {}
    for node in ids:
        d[node] = dict(dblp.nodes[node].items())
    return dict(nodes_data=d) 

@app.route("/get_node_data", methods=["POST", "GET"])
@unify
def get_one_data(dblp, data):
    lid = int(data["id"]) 
    return dict(data=dict(dblp.nodes[lid].items())) 

@app.route("/get_neighbors", methods=["POST", "GET"])
@unify
def get_neighbors(dblp, data):
    node = int(data["id"])
    return dict(data=[n for n in dblp[node]])

@app.route("/get_coauthors", methods=["POST", "GET"])
@unify
def get_coauthors(dblp, data):
    node = int(data["id"])
    S = set(k for n in dblp[node] for k in dblp[n])
    S.remove(node)
    return dict(data=list(S))

@app.route("/shortest_path", methods=["POST", "GET"])
@unify
def shortest_path(dblp, data):
    source = data.get("source", None)
    target = data.get("target", None)
    if source is None or target is None:
        raise ValueError("Must set both source=some_node_id and target=another_node_id")
    return dict(path=nx.shortest_path(dblp, int(source), int(target)))


@app.route("/kv_store", methods=["POST", "GET"])
def kv_store():
    if request.method == "POST":
        data = request.form
    else:
        data = request.args
    key = data.get("key")
    value = data.get("value")
    if key and value:
        with sqlite3.connect(kvpath) as db:
            try:
                db.execute("INSERT INTO key_value (key, value) VALUES (?, ?)", (key, value))
                return dict(input=data, validate=True)
            except Exception as E:
                return dict(input=data, validate=False, Exception=str(E)), 202
            
    elif value and not key:
        with sqlite3.connect(kvpath) as db:
            try:
                key = str(uuid.uuid4())
                db.execute("INSERT INTO key_value(key, value) VALUES (?, ?)", (key, value))
                return dict(input=data, validate=True, key=key)
            except Exception as E:
                return dict(input=data, validate=False, Exception=str(E)), 202
    elif key and not value:
        with sqlite3.connect(kvpath) as db:
            try:
                value = next(db.execute("SELECT value FROM key_value WHERE key=?", (key,)))[0]
                return dict(input=data, validate=True, key=key, value=value)
            except StopIteration as E:
                return dict(input=data, validate=False, Exception="key not set")
            except Exception as E:
                return dict(input=data, validate=False, Exception=[str(E), str(type(E))]), 202
    
    return dict(input=data, validate=False, Exception="key or value mandatory"), 202

